# baidumap-getpoint

# 使用地址（演示地址）

http://lovezengcheng.gitee.io/baidumap-getpoint/ | [【点击跟踪项目地址】](http://lovezengcheng.gitee.io/baidumap-getpoint/)


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
