import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import BaiduMap from 'vue-baidu-map'
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI);
Vue.config.productionTip = false

Vue.use(BaiduMap, {
  ak: '0tEsrmFKQIfoGFczUaxG8EIM1aFbDkNs'
})

new Vue({
  render: h => h(App),
}).$mount('#app')
